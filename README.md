## To Install

To get started you need gulp, grunt and bower installed then run `npm install -g grunt grunt-cli gulp bower` (grunt and bower are used by dependencies).

Get all the dependency packages with `npm install` then build.

To build all the 3rd party packages and put them in the **lib**  directory run `gulp install`.

You can run `gulp server` command to run the source on the localhost.
It will be deployed on http://localhost:3000