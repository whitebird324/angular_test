angular.module('mainController', [])
	.controller('main', function($scope, $state, $http) {
		$scope.title = 'Project name';

		$scope.listing = {};
		console.log(Date.now());
		var $promise=$http.get('https://www.reddit.com/r/all.json');
        $promise.then(function(res){
        	$scope.listing = res.data;
        	var diff;
        	for (var i = 0; i < $scope.listing.data.children.length; i ++) {
        		diff = timeDifference($scope.listing.data.children[i].data.created);
        		$scope.listing.data.children[i].data.difference = diff;
        	}
        });

	    var timeDifference = function(request) {
	    	return (Date.now() - request * 1000) / (1000*60*60);
	    }

	});
